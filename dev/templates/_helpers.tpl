{{/*
GLOBAL settings
=================================================================================================================== */}}

{{/*
Create chart name and version as used by the chart label.
----------------------------------------------------------------------------- */}}
{{- define "common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "common.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Common labels
----------------------------------------------------------------------------- */}}
{{- define "common.labels" -}}
app.kubernetes.io/version: {{ .Chart.Version }}-{{ .Chart.AppVersion }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ .Chart.name }}
environment: {{ .Values.appConfig.appEnv }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end }}

{{/*
Common Selector labels
----------------------------------------------------------------------------- */}}
{{- define "common.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
APP Settings
=================================================================================================================== */}}

{{/*
APP name
----------------------------------------------------------------------------- */}}
{{- define "common.app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}-app
{{- end }}

{{/*
APP Selector labels
----------------------------------------------------------------------------- */}}
{{- define "common.app.selectorLabels" -}}
{{ include "common.selectorLabels" . }}
app.kubernetes.io/name: {{ include "common.app.name" . }}
{{- end }}

{{/*
APP labels
----------------------------------------------------------------------------- */}}
{{- define "common.app.labels" -}}
{{ include "common.app.selectorLabels" . }}
{{- end }}


{{/*
DB Settings
=================================================================================================================== */}}

{{/*
DB name
----------------------------------------------------------------------------- */}}
{{- define "common.db.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}-db
{{- end }}

{{/*
DB Selector labels
----------------------------------------------------------------------------- */}}
{{- define "common.db.selectorLabels" -}}
{{ include "common.selectorLabels" . }}
app.kubernetes.io/name: {{ include "common.db.name" . }}
{{- end }}

{{/*
DB labels
----------------------------------------------------------------------------- */}}
{{- define "common.db.labels" -}}
{{ include "common.db.selectorLabels" . }}
{{- end }}
